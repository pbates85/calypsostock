package com.trident.calypsostock

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CalypsoStockApplication

fun main(args: Array<String>) {
    runApplication<CalypsoStockApplication>(*args)
}
